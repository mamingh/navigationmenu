const Init = {
    click: false
}

export default function menubtn (state = Init, action){
    switch (action.type) {
        case 'MENU_BUTTON':
            return {
                ...state,
                click: !state.click
            }
        default:
            return state
        }
    }