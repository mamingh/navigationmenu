import React from 'react'
import MenuRoundedIcon from '@material-ui/icons/MenuRounded';
import { connect } from 'react-redux'
import {menubtn} from './action/navigationmenu'

class MenuButton extends React.Component{
    constructor(){
        super()
        this.state = {
            click: false
        }
    }

    handleclick(){
        this.setState({
            click: true
        })
        this.props.dispatch(menubtn(this.state))
    }

    render(){
        return(
            <div className='menuButton'>
                <i><MenuRoundedIcon onClick={() => this.handleclick() } fontSize="large"/></i>
            </div>
        )
    }
}
const mapDispatchToProps = (dispatch) =>({
    dispatch: dispatch
})

export default connect(mapDispatchToProps)(MenuButton)