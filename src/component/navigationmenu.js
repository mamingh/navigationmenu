import React from 'react'
import {Spring} from 'react-spring/renderprops'

export default function NavigationMenu(){
        return(
                <Spring
                    from= {{opacity:0.5, marginRight:-100}}
                    to= {{opacity:1,marginRight:10}}
                    >
                    {props => (
                    <div style={props}>
                        <div class="container">
                            <div class="header">
                                <p>محمد حسین طهماسبی</p>
                            </div>
                            <div class="main">
                                <p>نمایش پروفایل</p>
                                <p>تراکنش ها</p>
                                <p>واریز ها</p>
                                <p>پشتیبانی</p>
                            </div>
                            <div class="logout">
                                <p>خروج از حساب کاربری</p>
                            </div>
                            <div class="footer">
                                <p>سو پی نسخه 1.0.0</p>
                            </div>
                        </div>
                    </div>
                    )}
                </Spring>
        )
}