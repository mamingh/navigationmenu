import React from 'react';
import './App.css';
import MenuButton from './component/menubutton'
import { connect } from 'react-redux'
import NavigationMenu from './component/navigationmenu'
import CSSTransitionGroup  from 'react-transition-group/CSSTransition'


class App extends React.Component{
  render(){
      return (
        <div>
          {this.props.data.click==false && <MenuButton />}
          {this.props.data.click==true &&
            <CSSTransitionGroup  
            transitionName="example"
            >
           <NavigationMenu/>
           </CSSTransitionGroup>
           }
        </div>
      )
    }
}


const mapStateToProps = (state) => ({
  data: state
})

export default connect(mapStateToProps)(App)

